object MainForm: TMainForm
  Left = 0
  Top = 0
  ClientHeight = 684
  ClientWidth = 947
  Caption = 'MainForm'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelLeft: TUniPanel
    Left = 0
    Top = 27
    Width = 130
    Height = 635
    Hint = ''
    Align = alLeft
    TabOrder = 0
    Caption = 'PanelLeft'
    Collapsible = True
    CollapseDirection = cdLeft
    AlignmentControl = uniAlignmentClient
    ParentAlignmentControl = False
    Layout = 'accordion'
    object PanelAccordion1: TUniPanel
      Left = 0
      Top = 0
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 1
      TitleVisible = True
      Title = 'PanelAccordion1'
      Caption = 'PanelAccordion1'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
      object TreeBase: TUniTreeView
        Left = 1
        Top = 1
        Width = 126
        Height = 98
        Hint = ''
        Items.NodeData = {
          0307000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
          000000000001048498A67E7B76B08B260000000000000000000000FFFFFFFFFF
          FFFFFF00000000000000000500000001043B531F75E55D5C4F26000000000000
          0000000000FFFFFFFFFFFFFFFF0000000000000000000000000104004E2C82C0
          68E567240000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
          000000010385511659D179240000000000000000000000FFFFFFFFFFFFFFFF00
          00000000000000000000000103944E985BD179240000000000000000000000FF
          FFFFFFFFFFFFFF0000000000000000000000000103E3535481D1792400000000
          00000000000000FFFFFFFFFFFFFFFF00000000000000000000000001038759A7
          4ED179260000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
          00000001043B60C068E55D5C4F260000000000000000000000FFFFFFFFFFFFFF
          FF00000000000000000000000001043665398DE55D5C4F260000000000000000
          000000FFFFFFFFFFFFFFFF0000000000000000000000000104DF7EA18BE567E2
          8B260000000000000000000000FFFFFFFFFFFFFFFF0000000000000000000000
          00010470656E63F47EA462260000000000000000000000FFFFFFFFFFFFFFFF00
          00000000000000000000000104FB7CDF7EA17B0674}
        Items.FontData = {
          0107000000FFFFFFFF00000000FFFFFFFF05000000FFFFFFFF00000000FFFFFF
          FF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFF
          FF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFF
          FF00000000}
        Align = alClient
        TabOrder = 1
        Color = clBtnFace
        OnClick = TreeBaseClick
      end
    end
    object PanelAccordion2: TUniPanel
      Left = 0
      Top = 102
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 2
      TitleVisible = True
      Title = 'PanelAccordion2'
      Caption = 'PanelAccordion2'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
    end
    object PanelAccordion3: TUniPanel
      Left = 0
      Top = 204
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 3
      TitleVisible = True
      Title = 'PanelAccordion3'
      Caption = 'PanelAccordion3'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
    end
    object PanelAccordion4: TUniPanel
      Left = 0
      Top = 306
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 4
      TitleVisible = True
      Title = 'PanelAccordion4'
      Caption = 'PanelAccordion4'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
    end
    object PanelAccordion5: TUniPanel
      Left = 0
      Top = 408
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 5
      TitleVisible = True
      Title = 'PanelAccordion5'
      Caption = 'PanelAccordion5'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
    end
    object PanelAccordion6: TUniPanel
      Left = 1
      Top = 510
      Width = 128
      Height = 100
      Cursor = crHandPoint
      Hint = ''
      TabOrder = 6
      TitleVisible = True
      Title = 'PanelAccordion6'
      Caption = 'PanelAccordion6'
      Layout = 'fit'
      LayoutConfig.Padding = '1'
    end
  end
  object PanelTop: TUniPanel
    Left = 0
    Top = 0
    Width = 947
    Height = 27
    Hint = ''
    Align = alTop
    TabOrder = 1
    Caption = ''
    CollapseDirection = cdTop
  end
  object PanelBottom: TUniPanel
    Left = 0
    Top = 662
    Width = 947
    Height = 22
    Hint = ''
    Align = alBottom
    TabOrder = 2
    Caption = 'PanelBottom'
    CollapseDirection = cdBottom
  end
  object PanelRight: TUniPanel
    Left = 840
    Top = 27
    Width = 107
    Height = 635
    Hint = ''
    Align = alRight
    TabOrder = 3
    Caption = 'PanelRight'
    Collapsible = True
    CollapseDirection = cdRight
    Collapsed = True
  end
  object SplitterLeft: TUniSplitter
    Left = 130
    Top = 27
    Width = 1
    Height = 635
    Hint = ''
    Align = alLeft
    ParentColor = False
    Color = clBtnFace
  end
  object PanelCenter: TUniPanel
    Left = 131
    Top = 27
    Width = 709
    Height = 635
    Hint = ''
    Align = alClient
    TabOrder = 5
    Caption = 'PanelCenter'
    object SplitteRight: TUniSplitter
      Left = 707
      Top = 1
      Width = 1
      Height = 633
      Hint = ''
      Align = alRight
      ParentColor = False
      Color = clBtnFace
    end
  end
  object PageControlMain: TUniPageControl
    Left = 176
    Top = 64
    Width = 465
    Height = 329
    Hint = ''
    ActivePage = UniTabSheet1
    TabOrder = 6
    object UniTabSheet1: TUniTabSheet
      Hint = ''
      Caption = 'UniTabSheet1'
    end
  end
  object UniLabel1: TUniLabel
    Left = 376
    Top = 33
    Width = 46
    Height = 13
    Hint = ''
    Caption = 'UniLabel1'
    TabOrder = 7
  end
end
