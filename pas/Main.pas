unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  uniGUITypes, uniGUIAbstractClasses, uniGUIClasses, uniGUIRegClasses,
  uniGUIForm, uniGUIBaseClasses, uniPanel, uniLabel, uniTreeView, uniToolBar,
  uniSplitter, uniMemo, uniPageControl,umain;

type
  TMainForm = class(TUniForm)
    PanelLeft: TUniPanel;
    PanelTop: TUniPanel;
    PanelBottom: TUniPanel;
    PanelRight: TUniPanel;
    SplitterLeft: TUniSplitter;
    PanelCenter: TUniPanel;
    SplitteRight: TUniSplitter;
    PanelAccordion1: TUniPanel;
    PanelAccordion2: TUniPanel;
    PanelAccordion3: TUniPanel;
    PanelAccordion4: TUniPanel;
    PanelAccordion5: TUniPanel;
    TreeBase: TUniTreeView;
    PanelAccordion6: TUniPanel;
    PageControlMain: TUniPageControl;
    UniLabel1: TUniLabel;
    UniTabSheet1: TUniTabSheet;
    procedure UniFormCreate(Sender: TObject);
    procedure TreeBaseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.TreeBaseClick(Sender: TObject);
var
  Nd: TUniTreeNode;
  Ts: TUniTabSheet;
//  FrC: TUniFrameClass;
//  Fr: TUniFrame;
  FClassName, Path: string;
begin
//  Path := UniServerModule.StartPath + 'units\';
//  Nd := NavTree.Selected;
//  if Nd.Count = 0 then
//  begin
//    Ts := Nd.Data;
//    if not Assigned(Ts) then
//    begin
//      Ts := TUniTabSheet.Create(Self);
//      Ts.PageControl := UniPageControl1;
//
//      Ts.Closable := True;
//      Ts.OnClose := TabSheetClose;
//      Ts.Tag := NativeInt(Nd);
//      Ts.Caption := Nd.Text;
//      Ts.ImageIndex := Nd.ImageIndex;
//
//      FClassName := 'TUni' + FileNames.Values[Nd.Text];
//
//      FrC := TUniFrameClass(FindClass(FClassName));
//
//      Fr := FrC.Create(Self);
//      Fr.Align := alClient;
//      Fr.Parent := Ts;
//
//      Nd.Data := Ts;
//    end;
//    UniPageControl1.ActivePage := Ts;
//  end;
end;

procedure TMainForm.UniFormCreate(Sender: TObject);
begin
  PanelAccordion1.Title:='体检系统(PEIS)';
  PanelAccordion2.Title:='信息系统(HIS)';
  PanelAccordion3.Title:='检验系统(LIS)';
  PanelAccordion4.Title:='影像系统(PACS)';
  PanelAccordion5.Title:='电子病历(EMR)';
  PanelAccordion6.Title:='系统设置(SETUP)';
end;

initialization
  RegisterAppFormClass(TMainForm);

end.

