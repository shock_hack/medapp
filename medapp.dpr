{$define UNIGUI_VCL} // Comment out this line to turn this project into an ISAPI module
                     // 如果想转换为ISAPI模式,注释掉此行

{$ifndef UNIGUI_VCL}
library
{$else}
program
{$endif}
  medapp;
  { Reduce EXE size by disabling as much of RTTI as possible (delphi 2009/2010) }
{$IF CompilerVersion >= 21.0}
{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}


uses
  uniGUIISAPI,
  Forms,
  ServerModule in 'pas\ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  MainModule in 'pas\MainModule.pas' {UniMainModule: TUniGUIMainModule},
  Main in 'pas\Main.pas' {MainForm: TUniForm},
  umain in 'pas\umain.pas' {FrameMain: TUniFrame};

{$R *.res}

{$ifndef UNIGUI_VCL}
exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;
{$endif}

begin
{$ifdef UNIGUI_VCL}
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  TUniServerModule.Create(Application);
  Application.Run;
{$endif}
end.
